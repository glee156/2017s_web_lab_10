-- Answers to Exercise 4 here
DROP TABLE IF EXISTS exercise04;

CREATE TABLE IF NOT EXISTS exercise04 (
  id int NOT NULL,
  title VARCHAR(50) NOT NULL,
  text TEXT,
  PRIMARY KEY (id)
);

INSERT INTO exercise04 (id, title, text) VALUES
  (1, "Carol", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat dignissim orci, sit amet suscipit lectus malesuada non. Vestibulum pellentesque, arcu non convallis aliquet, augue lectus blandit est, a egestas erat nulla at lorem. Morbi posuere eros a elit ullamcorper, vel euismod nibh consequat. Curabitur condimentum eu eros non tristique. Maecenas in egestas orci, a congue purus. Maecenas dapibus risus vitae risus condimentum, eget congue massa interdum. Nullam vel mi ac purus sollicitudin facilisis. Donec ultrices in nisi in tincidunt. Ut vehicula urna nisi, rhoncus lacinia nisi fringilla at. Pellentesque laoreet rhoncus libero eget faucibus. Proin auctor tempor nunc ut lacinia. Ut vulputate odio risus, a aliquet orci varius a. Quisque convallis nulla non convallis pretium. Nulla dapibus nisl a dolor euismod, eu pulvinar odio tempus. Aliquam efficitur venenatis ex, nec convallis est faucibus non. Nullam in arcu blandit, tempus arcu nec, dignissim tellus."),

  (15, "La la land", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat dignissim orci, sit amet suscipit lectus malesuada non. Vestibulum pellentesque, arcu non convallis aliquet, augue lectus blandit est, a egestas erat nulla at lorem. Morbi posuere eros a elit ullamcorper, vel euismod nibh consequat. Curabitur condimentum eu eros non tristique. Maecenas in egestas orci, a congue purus. Maecenas dapibus risus vitae risus condimentum, eget congue massa interdum. Nullam vel mi ac purus sollicitudin facilisis. Donec ultrices in nisi in tincidunt.")