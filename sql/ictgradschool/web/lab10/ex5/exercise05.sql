-- Answers to Exercise 5 here
DROP TABLE IF EXISTS exercise05;

CREATE TABLE IF NOT EXISTS exercise05 (
  username varchar(20) NOT NULL,
  first_name varchar(20) NOT NULL,
  last_name VARCHAR(20) NOT NULL,
  email VARCHAR(40),
  PRIMARY KEY (username)
);

INSERT INTO exercise05 (username, first_name, last_name, email) VALUES
  ("petey", "Pete", "Oscar", "petey@gmail.com"),
  ("peteroo", "Peter", "Lee", "peteroo@gmail.com"),
  ("programmer1", "Bill", "Gates", "bill@microsoft.com"),
  ("ziweir", "Nam", "Peterson", "ziweir@gmail.com"),
  ("petero", "Roo", "Roo", "peteroo@gmail.com");


