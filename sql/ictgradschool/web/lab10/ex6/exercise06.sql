-- Answers to Exercise 6 here
DROP TABLE IF EXISTS exercise06;
DROP TABLE IF EXISTS members;

CREATE TABLE IF NOT EXISTS members(
  memberName VARCHAR(20) NOT NULL,
  movieTitle VARCHAR(50) NOT NULL,
  PRIMARY KEY (movieTitle)
);

INSERT INTO members(memberName, movieTitle) VALUES
  ("Gayoung Lee", "Carol"),
  ("Thomas", "La la land"),
  ("Vincent", "Terminator"),
  ("Rose", "La Vie en Rose");


CREATE TABLE IF NOT EXISTS exercise06(
  id int NOT NULL,
  title VARCHAR(50) NOT NULL,
  director VARCHAR(30),
  rate int NOT NULL,
  FOREIGN KEY (title) REFERENCES members(movieTitle)

);

INSERT INTO exercise06(id, title, director, rate) VALUES
  (12, "Carol", "Bridget Hayman", 2),
  (10, "La la land", "Bridget Hayman", 4),
  (1, "Terminator", "Vince Schwartzman", 6),
  (3, "La vie en Rose", "Edith Prancer", 4);
