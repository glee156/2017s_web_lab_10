-- Answers to Exercise 7 here
DROP TABLE IF EXISTS comments;

CREATE TABLE comments(
  id int NOT NULL AUTO_INCREMENT,
  comment VARCHAR(500) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES exercise04(id)
);

INSERT INTO comments(comment) VALUES
  ("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
