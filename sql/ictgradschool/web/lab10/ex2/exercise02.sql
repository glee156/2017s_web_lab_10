-- Answers to Exercise 2 here
DROP TABLE IF EXISTS exercise02;

CREATE TABLE IF NOT EXISTS exercise02 (
  username varchar(20) NOT NULL,
  first_name varchar(20) NOT NULL,
  last_name VARCHAR(20) NOT NULL,
  email VARCHAR(40)
);

INSERT INTO exercise02 (username, first_name, last_name, email) VALUES
  ("petey", "Pete", "Oscar", "petey@gmail.com"),
  ("peteroo", "Peter", "Lee", "peteroo@gmail.com"),
  ("programmer1", "Bill", "Gates", "bill@microsoft.com"),
  ("ziweir", "Nam", "Peterson", "ziweir@gmail.com");