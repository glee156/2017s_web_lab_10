-- Answers to Exercise 8 here
DELETE FROM exercise05
WHERE username = 'petero';

ALTER TABLE exercise05 DROP COLUMN email;

DROP TABLE exercise05;

UPDATE exercise06
SET director="Hi", rate = 5
WHERE id = 1;

ALTER TABLE comments
    DROP FOREIGN KEY comments_ibfk_1;

UPDATE exercise05
SET username="imagurl"
WHERE username="petey";

UPDATE comments
SET id=77
WHERE id=1;