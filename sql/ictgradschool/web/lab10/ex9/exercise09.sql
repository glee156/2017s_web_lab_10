-- Answers to Exercise 9 here

-- All information about all the members of the video store
SELECT name
FROM exercise03;

-- Everything about all the members of the video store, except the number of video hires they have
SELECT name, gender, year_born, joined
FROM exercise03;

-- All the titles to the articles that have been written
SELECT title
FROM exercise04;

-- All the directors
SELECT director
FROM movies;

-- All the video titles that rent for $2 or less a week.
SELECT title
FROM movies
WHERE rate <= 2;

-- A sorted list of all the usernames that have been registered.
SELECT username
FROM exercise05;

-- All the usernames where the user’s first name starts with the letters ‘Pete’.
SELECT username
FROM exercise05
WHERE first_name LIKE 'Pete%';

-- All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
SELECT username
FROM exercise05
WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%'